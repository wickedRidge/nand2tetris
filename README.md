## Nand2tetris - nand2tetris.org - The Elements of Computing Systems MIT Press

These are the projects that I created using HDL and Ruby to make a computer from nand gates to abstract human thought.

Contents:

-Chips using HDL such as and, or, not, xor, adder, etc..
-Assembler
-VM Translator
-Compiler (almost complete)