#Author: Eric Gagnon
#Pair: Mike Mercer
#Date: Mar. 25, 2014

class CodeWriter 

  def initialize stream
   name = setFileName(stream) 
   @output = File.open(name, 'w') 
   @num = 0
   @otherNum = 0;  
  end
  def setFileName fileName 
   fileName.concat('.asm') 
     
  end
  def writeArithmetic cmd 
    case cmd 
    when "add"
      @output << ("@SP
        M=M-1
        A=M
        D=M
        @SP
        M=M-1
        A=M
        D=D+M
        @SP
        A=M
        M=D
        @SP
        M=M+1\n") 
    when "sub"
      @output << ("@SP
        M=M-1
        A=M
        D=M
        @SP
        M=M-1
        A=M
        D=M-D
        @SP
        A=M
        M=D
        @SP
        M=M+1\n") 
    when "neg"
      @output << ("@SP
        M=M-1
        A=M
        D=M
        M=-D
        @SP
        A=M
        M=D
        @SP
        M=M+1\n") 
  
    when "eq"
      @output << ("@SP
        M=M-1
        A=M
        D=M
        @SP
        M=M-1
        A=M
        D=D-M
        @eqfalse#{@num}  
        D;JNE  
        @SP
        A=M
        D=M
        M=-1
        @eqend#{@num} 
        0;JMP 
        (eqfalse#{@num}) 
        @SP
        A=M
        D=M
        M=0
        (eqend#{@num}) 
        @SP
        M=M+1\n") 
        @num+=1
    when "gt"
      @output << ("@SP
        M=M-1
        A=M
        D=M
        @SP
        M=M-1
        A=M
        D=D-M
        @gtfalse#{@num}
        D;JGE  
        @SP
        A=M
        D=M
        M=-1
        @gtend#{@num}
        0;JMP 
        (gtfalse#{@num}) 
        @SP
        A=M
        D=M
        M=0
        (gtend#{@num}) 
        @SP
        M=M+1\n") 
        @num+=1
    when "lt"
      @output << ("@SP
        M=M-1
        A=M
        D=M
        @SP
        M=M-1
        A=M
        D=D-M
        @ltfalse#{@num}
        D;JGT  
        @SP
        A=M
        D=M
        M=0
        @ltend#{@num}
        0;JMP 
        (ltfalse#{@num}) 
        @SP
        A=M
        D=M
        M=-1
        (ltend#{@num}) 
        @SP
        M=M+1\n")
        @num+=1 
    when "and"
      @output << ("@SP
        M=M-1
        A=M
        D=M
        @SP
        M=M-1
        A=M
        D=D&M
        M=D
        @SP
        M=M+1\n") 
    when "or"
      @output << ("@SP
        M=M-1
        A=M
        D=M
        @SP
        M=M-1
        A=M
        D=D|M
        M=D
        @SP
        M=M+1\n") 
    when "not"
      @output << ("@SP
        M=M-1
        A=M
        D=M
        M=!M
        @SP
        M=M+1\n") 
    else
      #Do Nothing
    end
  end
  def writePushPop cmd, seg, index 
    if cmd.eql? "C_PUSH"
      case seg
        when 'constant'  
          @output << "@#{index} 
            D=A
            @SP
            AM=M+1
            A=A-1
            M=D\n" 
        when 'local'
          @output << "@LCL
            D=M
            @#{index}
            D=D+A
            A=D
            D=M
            @SP
            A=M
            M=D
            @SP
            M=M+1\n"
        when 'that'
          @output << "@THAT
            D=M
            @#{index}
            D=D+A
            A=D
            D=M
            @SP
            A=M
            M=D
            @SP
            M=M+1\n"
        when 'argument'
          @output << "@ARG
            D=M
            @#{index}
            D=D+A
            A=D
            D=M
            @SP
            A=M
            M=D
            @SP
            M=M+1\n"
        when 'this'
          @output << "@THIS
            D=M
            @#{index}
            D=D+A
            A=D
            D=M
            @SP
            A=M
            M=D
            @SP
            M=M+1\n"
        when 'temp'
          @output << "@11
            D=M
            @SP
            A=M
            M=D
            @SP
            M=M+1\n"
        when 'pointer'
          if(index == '0')  
            @output << "@THIS
              D=M
              @SP
              A=M
              M=D
              @SP
              M=M+1\n"
          else
            @output << "@THAT
              D=M
              @SP
              A=M
              M=D
              @SP
              M=M+1\n"
          end
        when 'static'
          @output << "@StaticTest.vm.#{index}
            D=M
            @SP
            AM=M+1
            A=A-1
            M=D\n"
        else
          #Do nothing
      end    
    elsif cmd.eql? "C_POP"
        case seg
          when 'local'
            @output << "@LCL
              D=M
              @#{index}
              D=D+A
              @R13
              M=D
              @SP
              M=M-1
              A=M
              D=M
              @R13
              A=M
              M=D\n"    
          when 'argument'
            @output << "@ARG
              D=M
              @#{index}
              D=D+A
              @R13
              M=D
              @SP
              M=M-1
              A=M
              D=M
              @R13
              A=M
              M=D\n"
          when 'this'
            @output << "@THIS
              D=M
              @#{index}
              D=D+A
              @R13
              M=D
              @SP
              M=M-1
              A=M
              D=M
              @R13
              A=M
              M=D\n"
          when 'that'
            @output << "@THAT
              D=M
              @#{index}
              D=D+A
              @R13
              M=D
              @SP
              M=M-1
              A=M
              D=M
              @R13
              A=M
              M=D\n"
          when 'temp'
            @output << "@SP
              AM=M-1
              D=M
              @R5
              M=D\n"
          when 'pointer'
            if(index == '0')  
              @output << "@SP
                M=M-1
                A=M
                D=M
                @THIS
                M=D\n"
            else
              @output << "@SP
                M=M-1
                A=M
                D=M
                @THAT
                M=D\n"
            end
          when 'static'
            @output << "@SP
              M=M-1
              A=M
              D=M
              @#{index}
              M=D\n"
          else
            #Do Nothing
        end
    end
    @num += 1
  end

  def writeInit
    @output << "@256
    D=A
    @SP
    M=D\n"
    writeCall 'Sys.init', '0' 
  end

  def writeLabel label
    @output << "(#{label})\n"
  end

  def writeGoto label
    @output << "@#{label}
    0;JMP\n"
  end

  def writeIf label
    @output << "@SP
    M=M-1
    A=M
    D=M
    @#{label}
    D;JGT\n"
  end

  def writeCall name, numArgs
    numOffset = numArgs.to_i + 5
    @output << "@Sys.init.#{@otherNum}
    D=A
    @SP
    M=M+1
    A=M
    A=A-1
    M=D
    @LCL
    D=M
    @SP
    M=M+1
    A=M
    A=A-1
    M=D
    @ARG
    D=M
    @SP
    M=M+1
    A=M
    A=A-1
    M=D
    @THIS
    D=M
    @SP
    M=M+1
    A=M
    A=A-1
    M=D
    @THAT
    D=M
    @SP
    M=M+1
    A=M
    A=A-1
    M=D
    @SP
    D=M
    @#{numOffset}
    D=D-A
    @ARG
    M=D
    @SP
    D=M
    @LCL
    M=D
    @#{name}
    0;JMP\n"
    
    @output << "(Sys.init.#{@otherNum})\n" 
    @otherNum+=1
  end

  def writeReturn
    @output << "@LCL
    D=M
    @R14
    M=D
    @5
    A=D-A
    D=M
    @R13
    M=D
    @SP
    AM=M-1
    D=M
    @ARG
    A=M
    M=D
    @ARG
    D=M+1
    @SP
    M=D
    @R14
    AM=M-1
    D=M
    @THAT
    M=D
    @R14
    AM=M-1
    D=M
    @THIS
    M=D
    @R14
    AM=M-1
    D=M
    @ARG
    M=D
    @R14
    M=M-1
    A=M
    D=M
    @LCL
    M=D
    @R13
    A=M
    0;JMP\n"
  end

  def writeFunction name, numLocals
    writeLabel(name)
    (1..numLocals.to_i).each do
      @output << "@SP
      A=M
      M=D
      A=A+1
      @#{numLocals}
      D=A
      @SP
      M=D+M\n"
    end  
  end

  def close  
    @output.close 
  end 
     
end