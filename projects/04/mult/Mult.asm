// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm
// Author: Eric Gagnon
// Feb. 18, 2014


// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

// Put your code here.
	

@R0   //Set D = to the number of iterations
D=M

@i    //Set i = D
M=D

@R2		//Set product to 0
M=0


(LOOP)
	@i
	D=M
	@END
	D;JEQ  //If D==0 jump to end ie no iterations left

	@R1    //The number to be added
	D=M

	@R2    //The product 
	M=D+M

	@i     //Minus 1 from number of iterations
	M=M-1
	@LOOP
	0;JMP   //Return to beginning of loop


(END)