// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm
// Eric Gagnon
// PPP: Allison Diller and John Garo
// Date: Feb. 23, 2014


// Runs an infinite loop that listens to the keyboard input. 
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel. When no key is pressed, the
// program clears the screen, i.e. writes "white" in every pixel.

// Put your code here.
(LOOP) 			// Enter loop
	@COUNTER 	// Loads counter into active memory
	M=0 		// Counter points to first screen pixel

	@KBD 		// Take in user input
	D=M 		// Store user input value into D

	@BLACK		// Load BLACK method into memory
	D;JNE		// Jump to BLACK method if any key depressed



(WHITE) 		// WHITE method
	@COUNTER 	//LOAD COUNTER
	D=M 		//STORE COUNTER VALUE IN D
	@SCREEN 	//LOAD SCREEN
	A=A+D 		//ADD COUNTER TO CURRENT ADDRESS
	M=0 		//**CHANGE COLOR OF PIXEL
	@COUNTER 	//LOAD COUNTER
	M=M+1		//INCREMENT COUNTER
	D=M 		//STORE COUNTER VALUE IN D
	@SCREEN 	//LOAD SCREEN
	D=A+D 		//ADD SCREEN AND COUNTER
	@KBD 		//LOAD KEYBOARD
	D=D-A 		//FIND DIFFERENCE
	@WHITE 		//**LOAD WHITE METHOD
	D;JNE 		//TEST TO SEE IF VALUE REACHES KEYBOARD

	@LOOP
	0;JMP


(BLACK) 		// BLACK method
	
	@COUNTER 	//LOAD COUNTER
	D=M 		//STORE COUNTER VALUE IN D
	@SCREEN 	//LOAD SCREEN
	A=A+D 		//ADD COUNTER TO CURRENT ADDRESS
	M=-1 		//CHANGE COLOR OF PIXEL
	@COUNTER 	//LOAD COUNTER
	M=M+1		//INCREMENT COUNTER
	D=M 		//STORE COUNTER VALUE IN D
	@SCREEN 	//LOAD SCREEN
	D=A+D 		//ADD SCREEN AND COUNTER
	@KBD 		//LOAD KEYBOARD
	D=D-A 		//FIND DIFFERENCE
	@BLACK 		//LOAD BLACK METHOD
	D;JNE 		//TEST TO SEE IF VALUE REACHES KEYBOARD

	@LOOP
	0;JMP
