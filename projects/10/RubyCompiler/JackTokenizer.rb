#Author: Eric Gagnon and Mike Mercer
#Date: April 27, 2014
require './compilationengine'

class JackTokenizer
	@@keywords = ['class', 'constructor', 'function', 'method', 'field', 'static', 'var', 'int', 'char', 'boolean', 'void', 'true', 'false', 'null', 'this', 'let', 'do', 'if', 'else', 'while', 'return']
	@@symbols = ['{', '}','(',')','[',']', '.',',',';','+','-','*','/','&','|','<','>','=','~']

	def initialize input
		@fileIn = IO.readlines(input).select{|s| !(s.gsub(/\/\/.*/, '').strip.empty?)}.map { |s|
    s.gsub(/\/\/.*/, '').gsub(/\/(.*)/, '').gsub(/[*].+/, '').gsub(/^\s[*]/, '') }
		@fileIn.collect! do |line| 
			line.split(/(".*?"|[{}()\[\].,;+\-*\/&|<>=~]|\s+)/)
		end
		@fileIn.flatten!
		@fileIn.reject! {|line| line =~ /^\s*$/}
		@currentToken = ""
		@index = -1
	end

	def hasMoreTokens?
		@index + 1 < @fileIn.length
	end

	def advance
		@index+=1
		@currentToken = @fileIn[@index]
	end

	def typeToken
		if @@keywords.include?(@currentToken.downcase)
			"KEYWORD"
		elsif @@symbols.include?(@currentToken)
			"SYMBOL"		
		elsif @currentToken.match(/\d+/)
			"INT_CONST"
		elsif @currentToken.match(/^[a-zA-Z_][A-Za-z0-9_]*$/)
			"IDENTIFIER"
		else
			"STR_CONST"
		end
	end

	def keyWord
		return @currentToken if @@keywords.include? @currentToken
	end

	def symbol
		if(@@symbols.include? @currentToken)
			case @currentToken
			when '<'
				"&lt;"
			when '>'
				"&gt;"
			when '&'
				"&amp;"
			when "\""
				"&quot;"
			else
				@currentToken
			end
		end
	end

	def identifier
		@currentToken
	end

	def intVal
		@currentToken
	end

	def stringVal
		@currentToken.gsub(/\A"|"\Z/, '')
	end

end

#Main

files = []
dir = "../" + ARGV[0]
Dir.chdir(dir)
Dir.glob('*.jack').inject(0) do |count, file|
	files[count] = file
	count += 1
end
fileOut = files[0].dup
engine = CompilationEngine.new(files[0], fileOut)
engine.compileClass

files.each do |file|
end




