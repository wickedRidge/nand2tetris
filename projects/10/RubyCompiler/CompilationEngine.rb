#Author: Eric Gagnon and Mike Mercer
#Date: April 27, 2014

class CompilationEngine
	def initialize fileIn, fileOut
		@tokenizer = JackTokenizer.new(fileIn)
		@outfile = File.open(fileOut.gsub('.jack', '.xml'), 'w')
	end

	def compileClass
			@outfile << "<class>\n"
			while !@tokenizer.advance.match(/^(function|constructor|method)/)
				case @tokenizer.typeToken
				when "KEYWORD" 
					@outfile << "	<keyword> #{@tokenizer.keyWord} </keyword>\n"
				when "IDENTIFIER" 
					@outfile << "	<identifier> #{@tokenizer.identifier} </identifier>\n"
				when "SYMBOL" 
					@outfile << "	<symbol> #{@tokenizer.symbol} </symbol>\n"
				end
			end
			compileSubroutine

			@outfile << "</class>\n"
	end

	def compileClassVarDec
	end

	def compileSubroutine
		@outfile << "	<subroutineDec>\n"
		@outfile << "		<keyword> #{@tokenizer.keyWord} </keyword>\n"
		while !@tokenizer.advance.match(/\(/)
			case @tokenizer.typeToken
			when "KEYWORD" 
				@outfile << "		<keyword> #{@tokenizer.keyWord} </keyword>\n"
			when "IDENTIFIER" 
				@outfile << "		<identifier> #{@tokenizer.identifier} </identifier>\n"
			end
		end
		compileParameterList
		@outfile << "		<subroutineBody>\n"
		@tokenizer.advance
		@outfile << "			<symbol> #{@tokenizer.symbol} </symbol>\n"
		while @tokenizer.advance.match(/^var/)
			compileVarDec
		end
		if @tokenizer.keyWord.match(/^(do|let|while|if|return)/)
			until @tokenizer.advance.match(/^return/)
				compileStatements
			end
		end
	end

	def compileParameterList
		@outfile << "		<symbol> #{@tokenizer.symbol} </symbol>\n"
		@outfile << "		<parameterList>\n"
		@outfile << "		</parameterList>\n"
		@tokenizer.advance
		@outfile << "		<symbol> #{@tokenizer.symbol} </symbol>\n"
	end

	def compileVarDec
		@outfile << "		<varDec>\n"
		@outfile << "			<keyword> #{@tokenizer.keyWord} </keyword>\n"
		until @tokenizer.advance.match(/;/)
			case @tokenizer.typeToken
			when "KEYWORD" 
				@outfile << "			<keyword> #{@tokenizer.keyWord} </keyword>\n"
			when "IDENTIFIER" 
				@outfile << "			<identifier> #{@tokenizer.identifier} </identifier>\n"
			when "SYMBOL"
				@outfile << "			<symbol> #{@tokenizer.symbol} </symbol>\n"
			end
		end
		@outfile << "			<symbol> #{@tokenizer.symbol} </symbol>\n"
		@outfile << "		</varDec>\n"
	end

	def compileStatements
		@outfile << "		<statements>\n"
		case @tokenizer.keyWord
		when "do"
			compileDo
		when "let"
			compileLet
		when "while"
			compileWhile
		when "return"
			compileReturn
		when "if"
			compileIf
		end
		@outfile << "		</statements>\n"
	end

	def compileDo
		@outfile << "			<doStatement>\n"
		until @tokenizer.advance.match(/\(/)
		end
		@outfile << "			</doStatement>\n"
	end

	def compileLet
		@outfile << "			<letStatement>\n"
		until @tokenizer.advance.match(/\=/)
		end
		@outfile << "			</letStatement>\n"
	end

	def compileWhile
		@outfile << "			<whileStatement>\n"
		until @tokenizer.advance.match(/\(/)
		end
		@outfile << "			</whileStatement>\n"		
	end

	def compileReturn
		@outfile << "			<returnStatement>\n"
		@outfile << "			</returnStatement>\n"	
	end

	def compileIf
		@outfile << "			<ifStatement>\n"
		@outfile << "			</ifStatement>\n"	
	end

	def compileExpression
	end

	def compileTerm
	end

	def compileExpressionList
	end
end