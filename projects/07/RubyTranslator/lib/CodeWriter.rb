#Author: Eric Gagnon
#Date: Mar. 25, 2014

class CodeWriter 

  def initialize stream 
   @output = File.open(stream.gsub(".vm", ".asm"), 'w') 
   @num = 1  
  end
  def setFileName fileName 
   fileName.gsub!(".vm", ".asm") 
     
  end
  def writeArithmetic cmd 
    case cmd 
    when "add"
      @output << ("@SP
        M=M-1
        A=M
        D=M
        @SP
        M=M-1
        A=M
        D=D+M
        @SP
        A=M
        M=D
        @SP
        M=M+1\n") 
    when "sub"
      @output << ("@SP
        M=M-1
        A=M
        D=M
        @SP
        M=M-1
        A=M
        D=M-D
        @SP
        A=M
        M=D
        @SP
        M=M+1\n") 
    when "neg"
      @output << ("@SP
        M=M-1
        A=M
        D=M
        M=-D
        @SP
        A=M
        M=D
        @SP
        M=M+1\n") 
  
    when "eq"
      @output << ("@SP
        M=M-1
        A=M
        D=M
        @SP
        M=M-1
        A=M
        D=D-M
        @eqfalse#{@num}  
        D;JNE  
        @SP
        A=M
        D=M
        M=-1
        @eqend#{@num} 
        0;JMP 
        (eqfalse#{@num}) 
        @SP
        A=M
        D=M
        M=0
        (eqend#{@num}) 
        @SP
        M=M+1\n") 
        @num+=1
    when "gt"
      @output << ("@SP
        M=M-1
        A=M
        D=M
        @SP
        M=M-1
        A=M
        D=D-M
        @gtfalse#{@num}
        D;JGE  
        @SP
        A=M
        D=M
        M=-1
        @gtend#{@num}
        0;JMP 
        (gtfalse#{@num}) 
        @SP
        A=M
        D=M
        M=0
        (gtend#{@num}) 
        @SP
        M=M+1\n") 
        @num+=1
    when "lt"
      @output << ("@SP
        M=M-1
        A=M
        D=M
        @SP
        M=M-1
        A=M
        D=D-M
        @ltfalse#{@num}
        D;JGT  
        @SP
        A=M
        D=M
        M=0
        @ltend#{@num}
        0;JMP 
        (ltfalse#{@num}) 
        @SP
        A=M
        D=M
        M=-1
        (ltend#{@num}) 
        @SP
        M=M+1\n")
        @num+=1 
    when "and"
      @output << ("@SP
        M=M-1
        A=M
        D=M
        @SP
        M=M-1
        A=M
        D=D&M
        M=D
        @SP
        M=M+1\n") 
    when "or"
      @output << ("@SP
        M=M-1
        A=M
        D=M
        @SP
        M=M-1
        A=M
        D=D|M
        M=D
        @SP
        M=M+1\n") 
    when "not"
      @output << ("@SP
        M=M-1
        A=M
        D=M
        M=!M
        @SP
        M=M+1\n") 
    else
      #Do Nothing
    end
  end
  def writePushPop cmd, seg, index 
    if cmd.eql? "C_PUSH"
      case seg
        when 'constant'  
          @output << "@#{index} 
            D=A
            @SP
            A=M
            M=D
            @SP 
            M=M+1\n" 
        when 'local'
          @output << "@LCL
            D=M
            @#{index}
            D=D+A
            A=D
            D=M
            @SP
            A=M
            M=D
            @SP
            M=M+1\n"
        when 'that'
          @output << "@THAT
            D=M
            @#{index}
            D=D+A
            A=D
            D=M
            @SP
            A=M
            M=D
            @SP
            M=M+1\n"
        when 'argument'
          @output << "@ARG
            D=M
            @#{index}
            D=D+A
            A=D
            D=M
            @SP
            A=M
            M=D
            @SP
            M=M+1\n"
        when 'this'
          @output << "@THIS
            D=M
            @#{index}
            D=D+A
            A=D
            D=M
            @SP
            A=M
            M=D
            @SP
            M=M+1\n"
        when 'temp'
          @output << "@11
            D=M
            @SP
            A=M
            M=D
            @SP
            M=M+1\n"
        when 'pointer'
          if(index == '0')  
            @output << "@THIS
              D=M
              @SP
              A=M
              M=D
              @SP
              M=M+1\n"
          else
            @output << "@THAT
              D=M
              @SP
              A=M
              M=D
              @SP
              M=M+1\n"
          end
        when 'static'
          @output << "@#{index}
            D=M
            @SP
            A=M
            M=D
            @SP
            M=M+1\n"
        else
          #Do nothing
      end    
    elsif cmd.eql? "C_POP"
        case seg
          when 'local'
            @output << "@LCL
              D=M
              @#{index}
              D=D+A
              @R13
              M=D
              @SP
              M=M-1
              A=M
              D=M
              @R13
              A=M
              M=D\n"    
          when 'argument'
            @output << "@ARG
              D=M
              @#{index}
              D=D+A
              @R13
              M=D
              @SP
              M=M-1
              A=M
              D=M
              @R13
              A=M
              M=D\n"
          when 'this'
            @output << "@THIS
              D=M
              @#{index}
              D=D+A
              @R13
              M=D
              @SP
              M=M-1
              A=M
              D=M
              @R13
              A=M
              M=D\n"
          when 'that'
            @output << "@THAT
              D=M
              @#{index}
              D=D+A
              @R13
              M=D
              @SP
              M=M-1
              A=M
              D=M
              @R13
              A=M
              M=D\n"
          when 'temp'
            @output << "@SP
              M=M-1
              A=M
              D=M
              @11
              M=D\n"
          when 'pointer'
            if(index == '0')  
              @output << "@SP
                M=M-1
                A=M
                D=M
                @THIS
                M=D\n"
            else
              @output << "@SP
                M=M-1
                A=M
                D=M
                @THAT
                M=D\n"
            end
          when 'static'
            @output << "@SP
              M=M-1
              A=M
              D=M
              @#{index}
              M=D\n"
          else
            #Do Nothing
        end
    end
  end

  def writeInit
  end

  def writeLabel label
    @output << "(#{label})\n"
  end

  def writeGoto label
    @output << "@#{label}
    0;JMP\n"
  end

  def writeIf label
    @output << "@#{label}
    D;JGT\n"
  end

  def writeCall
  end

  def writeReturn
  end

  def writeFunction
  end

  def close  
    @output.close 
  end 
     
end