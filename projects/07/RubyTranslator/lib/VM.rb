#Author: Eric Gagnon
#Date: Mar. 25, 2014

require './parser'
require './codewriter'

class VM
  def initialize
    @file = IO.readlines(ARGV[0]).select{|s| !(s.gsub(/\/\/.*/, '').strip.empty?)}.map { |s|
    s.gsub(/\/\/.*/, '').strip } 
    @parser = Parser.new(@file)
    @code = CodeWriter.new(ARGV[0])
  end

  def run
    
    while @parser.hasMoreCommands
      @parser.advance
      case @parser.commandType
      when "C_PUSH"
        @code.writePushPop @parser.commandType, @parser.arg1, @parser.arg2
      when "C_POP"
        @code.writePushPop @parser.commandType, @parser.arg1, @parser.arg2
      when "C_ARITHMETIC"
        @code.writeArithmetic @parser.arg1
      when "C_IF"
        @code.writeIf @parser.arg1
      when "C_GOTO"
        @code.writeGoto @parser.arg1
      end
    end

    @code.close
  end
end

VM.new.run